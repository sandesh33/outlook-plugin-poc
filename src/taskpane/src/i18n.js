import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import translateEN from "../../assets/en.json";
import translateID from "../../assets/th.json";

const resources = {
  en: {
    translation: translateEN,
  },
  th: {
    translation: translateID,
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "en",
    // keySeparator:false, // We do not use keys in form messages.welcome
    interpolation: {
      escapeValue: false,
      skipOnVariables: true,
    },
  });

export default i18n;
