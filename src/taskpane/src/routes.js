import React from "react";
import { Switch, Route } from "react-router-dom";
// import Home from "./pages/home";
import TestPage from "./pages/test";
import CreateIssue from "./pages/create-issue";

export default function Routes() {
  return (
    <Switch>
      <Route path="/test" exact>
        <TestPage />
      </Route>
      <Route path="/add-issue" exact>
        <CreateIssue />
      </Route>
      <Route path="/">
        <TestPage />
      </Route>
    </Switch>
  );
}
