import React, { useEffect } from "react";
// import { TextField } from "@fluentui/react/lib/TextField";
// import { PrimaryButton } from "office-ui-fabric-react";
// import { useForm } from "react-hook-form";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function FormHook() {
  // const {
  //   register,
  //   handleSubmit,
  //   formState: { errors },
  // } = useForm();

  // const submit = (data) => {
  //   // eslint-disable-next-line no-undef
  //   console.log("???", data);
  // };
  // eslint-disable-next-line no-undef

  useEffect(() => {
    // eslint-disable-next-line no-undef
    let strin = localStorage.getItem("test");
    // eslint-disable-next-line no-undef
    console.log("????", strin);
  }, []);
  return (
    <>
      {/* <h1>Login form..</h1>
      <form onSubmit={handleSubmit(submit)}>
        <TextField label="Enter Email" type="text" {...register("email", { required: true, maxLength: 210 })} />
        {errors.email && <span role="alert">{errors.email.message}</span>}
        <TextField
          label="Password with reveal button"
          type="password"
          canRevealPassword
          revealPasswordAriaLabel="Show password"
          {...register("password", { required: true, maxLength: 210 })}
        />
        <PrimaryButton type="submit" text="Primary" />
      </form> */}
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
              <li>
                <Link to="/users">Users</Link>
              </li>
            </ul>
          </nav>

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/users">
              <Users />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
    </>
  );
}
export default FormHook;

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}
