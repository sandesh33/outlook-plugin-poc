import * as React from "react";
import PropTypes from "prop-types";
// import { Button, ButtonType } from "office-ui-fabric-react";
// import Header from "./Header";
// import HeroList from "./HeroList";
import Progress from "./Progress";
// import BasicForm from "./from";
// import FormHook from "./formhooks";
import { BrowserRouter } from "react-router-dom";
import NavBar from "./components/navigation";
import Routes from "./routes";
import { initializeIcons } from "@fluentui/font-icons-mdl2";
export default class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      listItems: [],
    };

    // eslint-disable-next-line no-undef
    console.log("This is test");
  }

  componentDidMount() {
    initializeIcons();
  }

  render() {
    const { title, isOfficeInitialized } = this.props;

    if (!isOfficeInitialized) {
      return (
        <Progress title={title} logo="assets/logo-filled.png" message="Please sideload your addin to see app body." />
      );
    }

    return (
      <div className="ms-welcome">
        <BrowserRouter>
          <NavBar />
          <Routes />
          {/* <FormHook formName="Login Form" /> */}
        </BrowserRouter>
      </div>
    );
  }
}

App.propTypes = {
  title: PropTypes.string,
  isOfficeInitialized: PropTypes.bool,
};
