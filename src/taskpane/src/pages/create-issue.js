import React, { useEffect } from "react";
// import { ChevronIcon } from "@fluentui/react-icons-mdl2";
import { IconButton } from "@fluentui/react/lib/Button";
import { Dropdown } from "@fluentui/react/lib/Dropdown";
import { TextField } from "@fluentui/react/lib/TextField";
import { useState } from "react";

const dropdownStyles = {
  dropdown: { width: 290 },
};

const projectOptions = [
  {
    name: "Story",
    icon: "🤷‍♀️",
    value: 1,
  },
  {
    name: "Task",
    icon: "🤷‍♀️",
    value: 2,
  },
  {
    name: "Bug",
    icon: "🤷‍♀️",
    value: 3,
  },
  {
    name: "Epic",
    icon: "🤷‍♀️",
    value: 4,
  },
];

export default function CreateIssue() {
  const [summary, setSummary] = useState("");
  const [description, setDescription] = useState("");

  const getMailItem = () => {
    // eslint-disable-next-line no-undef
    return Office.context.mailbox.item;
  };

  useEffect(() => {
    setSummary(getMailItem().subject);
    setDescription(
      // eslint-disable-next-line no-undef
      getMailItem().body.getAsync(Office.CoercionType.Text, (res) => {
        setDescription(res.value);
      })
    );

    // eslint-disable-next-line no-undef
    const gitHubUserName = Office.context.roamingSettings.get("gitHubUserName");
    // eslint-disable-next-line no-undef
    const defaultGistId = Office.context.roamingSettings.get("defaultGistId");

    // eslint-disable-next-line no-undef
    console.log("????", gitHubUserName, defaultGistId);
  }, []);

  return (
    <>
      <div className="header">
        <h3>Create New Issue</h3>
        <div className="action-buttons">
          <IconButton iconProps={{ iconName: "Settings" }} title="Add" ariaLabel="Add" />
        </div>
      </div>
      <div className="create-issue">
        <Dropdown
          placeholder="Select an option"
          label="Issue Type"
          required
          onChange={(e, item) => {
            // eslint-disable-next-line no-undef
            console.log("????", e.target, item);
          }}
          options={projectOptions}
          styles={dropdownStyles}
        />
        <TextField
          label="Summary"
          required
          placeholder="Please enter Summary"
          className="textFiled"
          value={summary}
          onChange={(e) => setSummary(e.target.value)}
        />
        <TextField
          label="Description"
          className="textFiled"
          value={description}
          multiline={true}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
        />
      </div>
    </>
  );
}
