import React, { useState } from "react";

export default function Home() {
  // console.log("?????", Office.context.roamingSettings.get("test"));
  const [issueId, setIssueId] = useState("");
  const login = () => {
    // eslint-disable-next-line no-undef
    const ref = window.open(
      "https://jira-poc-d76sjo2oha-el.a.run.app/oauth?state=sumit_agrawal&jiraHost=https://17sumitagrawal.atlassian.net",
      "_blank",
      "location=yes,height=570,width=520,scrollbars=yes,status=yes"
    );

  };

  const getIssues = () => {
    // const response = fetch(`https://jsonplaceholder.typicode.com/todos/1`)
    //   .then((response) => response.json())
    //   .then((json) => console.log(json));

    const response = fetch("https://jira-poc-d76sjo2oha-el.a.run.app/issue?issue=PROJ-4&state=sumit_agrawal", {
      mode: "cors",
      referrerPolicy: "strict-origin-when-cross-origin",
      body: null,
      method: "GET",
    });
    // eslint-disable-next-line no-undef
    response.then((res) => console.log(res));
  };

  return (
    <div>
      home
      <button onClick={login}>Login</button>
      <input onChange={(e) => setIssueId(e.target.value)} />
      <button onClick={getIssues}>Get Issues</button>
    </div>
  );
}
