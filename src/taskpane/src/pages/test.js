/* eslint-disable no-undef */
import React, { useEffect, useState } from "react";
import { Button, Input, message, Modal } from "antd";
import CommonTest from "../../../common/check";
import Home from "./home";
const { Search } = Input;

export default function TestPage() {
  const [visible, setVisibility] = useState(false);

  useEffect(() => {
    // saveSettings();
  }, []);

  const saveSettings = () => {
    Office.context.roamingSettings.set("gitHubUserName", "test Username");
    Office.context.roamingSettings.set("defaultGistId", "Test id");

    Office.context.roamingSettings.saveAsync((res) => {
      console.log("res", res);
    });
  };

  const showModal = () => {
    setVisibility(true);
  };

  const handleOk = () => {
    setVisibility(false);
  };

  const handleCancel = () => {
    setVisibility(false);
  };
  return (
    <>
      <h1>This is Test page</h1>
      <CommonTest />
      <Home />
      <button
        onClick={() => {
          // eslint-disable-next-line no-undef
          window.location.reload();
        }}
      >
        Reload
      </button>
      <button
        onClick={() => {
          // eslint-disable-next-line no-undef
          Office.context.ui.displayDialogAsync("https://localhost:3000/taskpane.html");
        }}
      >
        Open in Modal
      </button>

      <button
        onClick={() => {
          // eslint-disable-next-line no-undef
          showModal();
        }}
      >
        Open in Modal1
      </button>

      <button
        onClick={() => {
          // eslint-disable-next-line no-undef
          message.success("This is a success message");
        }}
      >
        Show alert
      </button>

      <Input size="large" placeholder="large size" />
      <Input placeholder="default size" />
      <Input size="small" placeholder="small size" />

      <Search placeholder="input search loading with enterButton" loading enterButton />

      <Button type="primary">Primary</Button>

      <Modal title="Basic Modal" visible={visible} onOk={handleOk} onCancel={handleCancel}>
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
      <button type="button" className="btn btn-primary">
        Primary
      </button>
    </>
  );
}
