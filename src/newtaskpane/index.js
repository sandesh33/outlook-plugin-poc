import "office-ui-fabric-react/dist/css/fabric.min.css";
import App from "./components/App";
import { AppContainer } from "react-hot-loader";
import { initializeIcons } from "office-ui-fabric-react/lib/Icons";
import * as React from "react";
import * as ReactDOM from "react-dom";
/* global document, Office, module, require */
initializeIcons();

let isOfficeInitialized = false;

const title = "Contoso Task Pane Add-in?????";

// eslint-disable-next-line no-undef
console.log("This is test");

const render = (Component) => {
  // eslint-disable-next-line no-undef
  console.log("This is test");

  ReactDOM.render(
    <AppContainer>
      <Component title={title} isOfficeInitialized={isOfficeInitialized} />
    </AppContainer>,
    document.getElementById("container")
  );
};

/* Render application after Office initializes */
Office.initialize = () => {
  isOfficeInitialized = true;
  render(App);
};

/* Initial render showing a progress bar */
render(App);

if (module.hot) {
  module.hot.accept("../taskpane/components/App", () => {
    const NextApp = require("../taskpane/components/App").default;
    render(NextApp);
  });
}
